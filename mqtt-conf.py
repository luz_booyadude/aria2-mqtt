MQTT_CLIENT_ID = "cr1tikal-pi"
MQTT_BROKER = "iot.eclipse.org"
MQTT_PORT = 443
MQTT_CLEAN_SESSION = False  # true for discard all data upon disconnect, false otherwise
# MQTT_PROTOCOL = MQTTv311
MQTT_TRANSPORT = "websockets"