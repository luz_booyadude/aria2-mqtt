import paho.mqtt.client as mqtt
# from subprocess import call
import os
import sys
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, dir_path + '/pyaria2')
import pyaria2 as aria2
# from debug import *

MQTT_CLIENT_ID = "cr1tikal-pi"
MQTT_BROKER = "broker.hivemq.com"
MQTT_PORT = 1883
MQTT_CLEAN_SESSION = False  # true for discard all data upon disconnect, false otherwise
# MQTT_PROTOCOL = MQTTv311
# MQTT_TRANSPORT = "websockets"
MQTT_TOPIC_REQ = "aria2/req/"
MQTT_TOPIC_RESP = "aria2/resp"
MQTT_TOPIC_ADD = "aria2/req/add"
MQTT_TOPIC_STOP = "aria2/req/stop"
MQTT_TOPIC_PAUSE = "aria2/req/pause"
MQTT_TOPIC_PAUSEALL = "aria2/req/pauseall"
MQTT_TOPIC_RESUME = "aria2/req/resume"
MQTT_TOPIC_RESUMEALL = "aria2/req/resumeall"
MQTT_TOPIC_INFO_REQ_ACTIVE = "aria2/req/info/active"
MQTT_TOPIC_INFO_REQ_PAUSE = "aria2/req/info/pause"
MQTT_TOPIC_INFO_REQ_STOP = "aria2/req/info/stop"
MQTT_TOPIC_INFO_RESP = "aria2/resp/info/active"

MQTT_TOPIC_SUBSCRIBE = [
    (MQTT_TOPIC_REQ, 0),
    (MQTT_TOPIC_STOP, 0),
    (MQTT_TOPIC_PAUSE, 0),
    (MQTT_TOPIC_PAUSEALL, 0),
    (MQTT_TOPIC_RESUME, 0),
    (MQTT_TOPIC_RESUMEALL, 0),
    (MQTT_TOPIC_INFO_REQ_ACTIVE, 0),
    (MQTT_TOPIC_INFO_REQ_PAUSE, 0),
    (MQTT_TOPIC_INFO_REQ_STOP, 0),
]

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    if rc is 0:
        print("Connected successfully!")
    elif rc is 1:
        print("ERROR: Incorrect protocol version!")
    elif rc is 2:
        print("ERROR: Invalid client identifier!")
    elif rc is 3:
        print("ERROR: Server unavailable!")
    elif rc is 4:
        print("ERROR: Bad username or password!")
    elif rc is 5:
        print("ERROR: Not authorised!")
    else:
        print("ERROR: Unknown error!")
    # print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(MQTT_TOPIC_SUBSCRIBE)

    # Try publish something once connected
    client.publish(MQTT_TOPIC_RESP, "Hey, Im connected!")

def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    if msg.topic == MQTT_TOPIC_REQ:
        print(str(msg.payload))
    elif msg.topic == MQTT_TOPIC_ADD:
        link = msg.payload.decode("utf-8")
        print("Adding new download: " + link)
        uri = []
        uri = link.split(',')
        # uri.append(link)
        aria2rpc.addUri(uri)
        client.publish(MQTT_TOPIC_RESP, "New download succussfully added!")
    elif msg.topic == MQTT_TOPIC_PAUSE:
        link = msg.payload.decode("utf-8")
        print("Pausing download " + link)
        aria2rpc.pause(uri)
    elif msg.topic == MQTT_TOPIC_PAUSEALL:
        link = msg.payload.decode("utf-8")
        print("Pausing all download")
        aria2rpc.pauseAll()
    elif msg.topic == MQTT_TOPIC_RESUMEALL:
        print("Resuming all download")
        aria2rpc.unpauseAll()
    elif msg.topic == MQTT_TOPIC_INFO_REQ_ACTIVE:
        info = str(aria2rpc.tellActive())
        client.publish(MQTT_TOPIC_INFO_RESP, info)
    elif msg.topic == MQTT_TOPIC_INFO_REQ_PAUSE:
        info = str(aria2rpc.tellWaiting())
        client.publish(MQTT_TOPIC_INFO_RESP, info)
    elif msg.topic == MQTT_TOPIC_INFO_REQ_STOP:
        info = str(aria2rpc.tellStopped())
        client.publish(MQTT_TOPIC_INFO_RESP, info)
    # print(msg.topic+" "+str(msg.payload))
    # cmd = msg.payload.decode("utf-8")
    # if cmd is '1':
    #     print(cmd)
    # else:
    #     print(msg.topic+" "+str(msg.payload))
    # debug("Message received from topic " + msg.topic)
    # debug("Message content: " + cmd)
    # call(["sudo", "/home/pi/repo/ArduiPi_OLED/modules/billboard", "-m", cmd])
    # print(cmd)

def on_publish(client, userdata, mid):
    print(str(mid))

client = mqtt.Client(MQTT_CLIENT_ID)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message
client.on_publish = on_publish
client.connect_async(MQTT_BROKER, MQTT_PORT, 60)
client.loop_start()  # use loop_start when start async

aria2rpc = aria2.PyAria2()
# aria2rpc.addUri(uri)
while 1:
    pass  # do some logic here